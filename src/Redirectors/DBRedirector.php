<?php

namespace Thyseus\DBRedirector\Redirectors;

use Thyseus\DBRedirector\Models\Redirect;
use Symfony\Component\HttpFoundation\Request;
use Spatie\MissingPageRedirector\Redirector\Redirector;

/**
 * Class DBRedirector
 *
 * @package Thyseus\DBRedirector\Redirectors
 */
class DBRedirector implements Redirector
{
    /**
     * Read the configured redirects from the database using the
     * Redirect Eloquent Model.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return array
     */
    public function getRedirectsFor(Request $request): array
    {
        $redirections = [];

        foreach (Redirect::query()
                     ->where('active', true)
                     ->get() as $redirect) {
            $redirections[$redirect->old] = $redirect->new;
        }

        return $redirections;
    }
}