@extends('DBRedirector::layouts.redirector')

@section('content')
    <section>
        <form method="POST" action="{{ $redirect->id === null
                ? route('redirects.store')
                : route('redirects.update', ['redirect' => $redirect]) }}">
            @csrf
            @if ($redirect->id !== null)
                @method('PUT')
            @endif
            <header>
                <h2> Redirection Rule</h2>
            </header>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <label for="old">Old:</label>
            <input
                type="text"
                required="required"
                id="old"
                name="old"
                size="40"
                value="{{ $redirect->old ?? old('old') }}"
                placeholder="old url"
            >

            <label for="new">New:</label>
            <input
                type="text"
                required="required"
                id="new"
                name="new"
                size="40"
                value="{{ $redirect->new ?? old('new') }}"
                placeholder="new url"
            >

            <label for="code">Code:</label>
            <select id="code" name="code">
                <option {{ $redirect->code === '301' ? 'selected' : '' }} value="301">301</option>
                <option {{ $redirect->code === '302' ? 'selected' : '' }} value="302">302</option>
            </select>

            <label for="active">Active:</label>
            <select id="active" name="active">
                <option {{ $redirect->active === 0 ? 'selected' : '' }} value="0">Inactive</option>
                <option {{ $redirect->active === 1 ? 'selected' : '' }} value="1">Active</option>
            </select>

            <label for="comment">Comment (only for internal use):</label>

            <textarea cols="40"
                      rows="5"
                      id="comment"
                      name="comment">{{ $redirect->comment ?? old('comment') }}
            </textarea>

            <a href=" {{ route ('redirects.index') }}">
                <b> Back (discard changes) </b>
            </a>
            <button type="submit">Save Redirect Rule</button>
        </form>
    </section>
@endsection