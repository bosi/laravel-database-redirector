@extends('DBRedirector::layouts.redirector')

@section('content')
    <h2>Manage Redirections</h2>

    @if (count($redirects) > 0)
        <table class="table">
            <thead>
            <tr>
                <th><a href="?order_by=old">Old</a></th>
                <th><a href="?order_by=new">New</a></th>
                <th><a href="?order_by=code">Code</a></th>
                <th><a href="?order_by=active">Active</a></th>
                <th><a href="?order_by=created_at">Created at</a></th>
                <th><a href="?order_by=updated_at">Updated at</a></th>
                <th> Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($redirects as $redirect)
                <tr>
                    <td> {{ $redirect->old }} </td>
                    <td> {{ $redirect->new }} </td>
                    <td> {{ $redirect->code }} </td>
                    <td> {{ $redirect->active ? 'Yes' : 'No' }} </td>
                    <td> {{ $redirect->created_at }}</td>
                    <td> {{ $redirect->updated_at }}</td>
                    <td>

                        <form
                            action="{{ route('redirects.destroy', [ 'redirect' => $redirect ]) }}"
                            method="POST"
                            style="border: 0;padding:0;box-shadow:0;text-align:center;"
                        >
                            @csrf
                            @method('DELETE')
                            <a
                                href="{{ route('redirects.edit', ['redirect' => $redirect]) }}"><b>Edit</b></a>
                            <button type="submit" value="Delete">Delete</button>
                        </form>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    @else
        <p> You have no redirects configured yet.</p>
    @endif

    <p>
        <a href="{{ route('redirects.create') }}">
            <b> Create new redirect Rule </b>
        </a>
    </p>
@endsection