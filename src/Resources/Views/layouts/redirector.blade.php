<html>
<head>
    <title>Laravel Redirector Administration Backend </title>
    <link rel="stylesheet" href="/vendor/db-redirector/mvp.css">
</head>
<body>

<div class="container">
    @yield('content')
</div>
</body>
</html>
