<?php

namespace Thyseus\DBRedirector\Controllers;

use App\Events\RedirectCreated;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Thyseus\DBRedirector\Requests\RedirectStoreRequest;
use Thyseus\DBRedirector\Models\Redirect;
use Thyseus\DBRedirector\Requests\RedirectIndexRequest;
use Thyseus\DBRedirector\Providers\RedirectorServiceProvider;

/**
 * Class RedirectController
 *
 * @package Thyseus\DBRedirector\Controllers
 */
class RedirectController extends Controller
{
    public const NAMESPACE = RedirectorServiceProvider::NAMESPACE;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RedirectIndexRequest $request)
    {
        $orderBy = request()->get('order_by') ?? 'created_at';

        $redirects = Redirect::query()->orderBy($orderBy)->get();

        return view(self::NAMESPACE . '::index', [
            'redirects' => $redirects,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Redirect $redirect)
    {
        return view(self::NAMESPACE . '::form', [
            'redirect' => $redirect,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function edit(Redirect $redirect)
    {
        return view(self::NAMESPACE . '::form', [
            'redirect' => $redirect,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function store(RedirectStoreRequest $request)
    {
        $redirect = Redirect::create($request->all());

        $redirect->save();

        $request->session()->flash('success', 'The redirection rule has been created');

        return redirect()->route('redirects.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RedirectStoreRequest $request, Redirect $redirect)
    {
        $redirect->update($request->all());

        return redirect()
            ->route('redirects.index')
            ->with('success', 'Redirect updated successfully');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Redirect            $redirect
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Redirect $redirect)
    {
        $redirect->delete();

        return redirect()
            ->route('redirects.index')
            ->with('success', 'The redirection rule has been deleted');
    }
}
