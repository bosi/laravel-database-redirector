<?php

namespace Thyseus\DBRedirector\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RedirectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old'     => 'required|max:255',
            'new'     => 'required|max:255',
            'code'    => Rule::in(['301', '302']),
            'active'  => Rule::in(['0', '1']),
            'comment' => 'max:4096',
        ];
    }
}
