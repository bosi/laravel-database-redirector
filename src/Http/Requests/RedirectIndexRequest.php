<?php

namespace Thyseus\DBRedirector\Requests;

use Illuminate\Validation\Rule;
use Thyseus\DBRedirector\Models\Redirect;
use Illuminate\Foundation\Http\FormRequest;

class RedirectIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_by' => Rule::in([
                'old', 'new', 'code', 'active', 'created_at', 'created_by',
            ]),
        ];
    }
}
