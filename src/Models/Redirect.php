<?php

namespace Thyseus\DBRedirector\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Redirect
 *
 * @property string old
 * @property string new
 * @property string code
 * @property string active
 * @property string comment
 *
 * @package Thyseus\DBRedirector\Models
 */
class Redirect extends Model
{
    public $fillable = ['old', 'new', 'code', 'active', 'comment'];
}
