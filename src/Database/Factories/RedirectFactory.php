<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Thyseus\DBRedirector\Models\Redirect;
use Faker\Generator as Faker;

$factory->define(Redirect::class, function (Faker $faker) {
    return [
        'old'     => $faker->slug,
        'new'     => $faker->slug,
        'active'  => 1,
        'code'    => $faker->randomElement(['301', '302']),
        'comment' => $faker->sentence,
    ];
});
