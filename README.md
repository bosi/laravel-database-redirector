# laravel-database-redirector

This package provides an Database Redirector and (very) simple Admin Backend for:
https://github.com/spatie/laravel-missing-page-redirector

## Screenshots

![First Screenshot of Laravel Database Redirector](docs/laravel-database-redirector-screenshot1.png "Index view")
![Second Screenshot of Laravel Database Redirector](docs/laravel-database-redirector-screenshot2.png "Create/Update view")

## Prerequisites

For this package you will need:

* an laravel 6.x or newer application
* a configured database
* an middleware named `admin` which controls the admin access

## Installation

* run: `composer require thyseus/laravel-database-redirector`
* run: `php artisan vendor:publish --provider="Spatie\MissingPageRedirector\MissingPageRedirectorServiceProvider"`
* run: `php artisan vendor:publish --tag=public --force`
* run: `php artisan migrate`

* edit your `config/missing-page-redirector.php` and set `'redirector'` to `\Thyseus\DBRedirector\Redirectors\DBRedirector::class`
* access `http://localhost/admin/redirects` to administrate the redirections of your application.

## Hints

By default, only 404s will be redirected. If you want the laravel-missing-page-redirector to
redirect every route configured, you will need to set the `'redirect_status_codes'` config
to an empty array.

Thanks a lot to the MVP css framework (https://github.com/andybrewer/mvp/) which is bundled with
this package to make the backend at least a little bit appealing :)

## Tests

In order to do automated tests of this package, you will need to use the 
codeception (https://codeception.com/) testing framework in your application. You can copy
the provided functional test in tests/Functional/RedirectsCest.php in your Functional test suite
or use it as a boilerplate to do your own tests. 

## See also:

* https://laravel-news.com/missing-pages-redirector
* https://freek.dev/598-retain-your-seo-worth-by-correctly-redirecting-missing-pages-in-a-laravel-application
