<?php

use Thyseus\DBRedirector\Models\Redirect;

/**
 * Class RedirectsCest
 *
 * To use this test you need to use codeception.
 * Place this file in your applications Functional Suite.
 */
class RedirectsCest
{
    public function _before(FunctionalTester $I)
    {
        $I->loginAsAdmin();
    }

    public function index(FunctionalTester $I)
    {
        $redirectAAA = $I->have(Redirect::class, [
            'created_at' => '2020-01-02',
            'old'        => 'AAA',
        ]);

        $redirectZZZ = $I->have(Redirect::class, [
            'created_at' => '2020-01-01',
            'old'        => 'ZZZ',
        ]);

        $I->amOnRoute('redirects.index');

        $firstRow = '.table > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)';

        $I->see($redirectZZZ->old, $firstRow);
        $I->see($redirectZZZ->new);
        $I->see($redirectZZZ->code);
        $I->see('Yes'); // $redirect->active

        $I->see($redirectAAA->old);
        $I->see($redirectAAA->new);
        $I->see($redirectAAA->code);
        $I->see('Yes'); // $redirect->active

        // edit button:
        $I->canSeeElement('a', [
            'href' => "http://localhost/admin/redirects/{$redirectAAA->id}/edit",
        ]);

        // delete button:
        $I->canSeeElement('form', [
            'action' => "http://localhost/admin/redirects/{$redirectAAA->id}",
        ]);

        // test sorting:
        $I->click('Old');

        $I->see($redirectAAA->old, $firstRow);
    }

    public function create(FunctionalTester $I)
    {
        $I->amOnRoute('redirects.create');

        $faker = \Faker\Factory::create();

        $old = $faker->slug;
        $new = $faker->slug;
        $code = '301';
        $comment = $faker->sentence;

        $I->fillField('old', $old);
        $I->fillField('new', $new);
        $I->fillField('comment', $comment);

        $I->selectOption('code', $code);
        $I->selectOption('active', 1);

        $I->click('Save Redirect Rule');

        $I->seeRecord(Redirect::class, [
            'old'     => $old,
            'new'     => $new,
            'code'    => $code,
            'comment' => $comment,
            'active'  => 1,
        ]);

        // after being redirected to index, :
        $I->see($old);
        $I->see($new);
        $I->see($code);
    }

    public function edit(FunctionalTester $I)
    {
        $redirect = $I->have(Redirect::class);

        $faker = \Faker\Factory::create();

        $I->amOnRoute('redirects.edit', ['redirect' => $redirect]);

        $new = $faker->slug;

        $I->fillField('new', $new);

        $I->click('Save Redirect Rule');

        $I->seeRecord(Redirect::class, [
            'id'     => $redirect->id,
            'old'    => $redirect->old,
            'new'    => $new,
            'code'   => $redirect->code,
            'active' => $redirect->active,
        ]);

        $I->dontSeeRecord(Redirect::class, [
            'new' => $redirect->new,
        ]);

        // after being redirected to index, :
        $I->see($redirect->old);
        $I->see($new);
        $I->see($redirect->code);
    }

    public function delete(FunctionalTester $I)
    {
        $redirect = $I->have(Redirect::class);

        $I->amOnRoute('redirects.index');

        // only button on page:
        $I->click('Delete');

        $I->dontSeeRecord(Redirect::class, [
            'id' => $redirect->id,
        ]);

        $I->dontSee($redirect->old);
    }
}
